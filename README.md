# Late Night Humor Joke Parser

This program parses text files of jokes downloaded from LexisNexis Academic into a H2GIS database for consumption in mapping software.

Jokes are designed to be pulled from The Bulletin's Frontrunner Last Laughs section.

After being parsed, each jokeComponent is analyzed by [MIT's CLIFF geoparsing tool](https://github.com/mitmedialab/CLIFF) and the data is stored relationally in the database.
