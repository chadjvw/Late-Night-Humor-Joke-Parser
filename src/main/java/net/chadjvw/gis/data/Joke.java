package net.chadjvw.gis.data;

import java.util.Date;

/**
 * Created by Chad on 5/18/2016.
 */
public class Joke extends Component
{
	private String joke;
	private Date date;

	public Joke(String joke, Date date)
	{
		this.joke = joke;
		this.date = date;
	}

	@Override
	public String getJoke()
	{
		return joke;
	}

	@Override
	public Date getDate()
	{
		return date;
	}

	@Override
	public void parse()
	{

	}

	@Override
	public String toString()
	{
		return "Joke{" +
				"joke='" + joke + '\'' +
				", date=" + date +
				'}';
	}
}
