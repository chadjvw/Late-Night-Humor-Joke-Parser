package net.chadjvw.gis.data;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Chad on 5/18/2016.
 */
public class Comedian extends Component
{
	Set<Component> jokes = new HashSet<>();

	private String name;

	public Comedian(String name)
	{
		this.name = name;
	}

	@Override
	public void add(Component component)
	{
		jokes.add(component);
	}

	@Override
	public void remove(Component component)
	{
		jokes.remove(component);
	}

	@Override
	public String getName()
	{
		return name;
	}

	@Override
	public String toString()
	{
		return "Comedian{" +
				"jokes=" + jokes +
				", name='" + name + '\'' +
				'}';
	}
}
