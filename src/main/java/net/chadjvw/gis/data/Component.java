package net.chadjvw.gis.data;

import java.util.Date;

/**
 * Created by Chad on 5/18/2016.
 */
public abstract class Component
{
	public void add(Component component)
	{
		throw new UnsupportedOperationException();
	}

	public void parse()
	{
		throw new UnsupportedOperationException();
	}

	public void remove(Component component)
	{
		throw new UnsupportedOperationException();
	}

	public Component getChild(int i)
	{
		throw new UnsupportedOperationException();
	}

	public String getName()
	{
		throw new UnsupportedOperationException();
	}

	public String getJoke()
	{
		throw new UnsupportedOperationException();
	}

	public Date getDate()
	{
		throw new UnsupportedOperationException();
	}

	public void execute()
	{
		throw new UnsupportedOperationException();
	}
}
