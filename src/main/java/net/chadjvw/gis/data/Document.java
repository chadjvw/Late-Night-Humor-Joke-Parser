package net.chadjvw.gis.data;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Created by Chad on 5/12/2016.
 */
public class Document
{
	private static final Logger log = LogManager.getLogger(Document.class);
	private String documentText;
	private Component comedians;
	private LocalDate date;
	private final Pattern p = Pattern
			.compile("((?:(?:jan|feb)?r?(?:uary)|mar(?:ch)|apr(?:il)|may|june?|july?|aug(?:ust)|oct(?:ober)|(?:sept?|nov|dec)(?:ember))\\s+\\d{1,2}\\s*,?\\s*\\d{4}),?\\s((?:(?:Mon)|(?:Tues?)|(?:Wed(?:nes)?)|(?:Thur?s?)|(?:Fri)|(?:Sat(?:ur)?)|(?:Sun))(?:day))?", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.UNIX_LINES);
	private final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MMMMM dd, yyyy");

	public Document(String documentText)
	{
		this.documentText = documentText;
		setDate(documentText);
	}

	public String getDocumentText()
	{
		return documentText;
	}

	public LocalDate getDate()
	{
		return date;
	}

	private void setDate(String documentText)
	{


		Matcher m = p.matcher(documentText);

		String regexDate = null;

		try
		{
			m.find();
			regexDate = m.group(1);
			this.date = LocalDate.parse(regexDate, dtf);

			if (regexDate == null)
			{
				throw new Exception("There was an error trying to process the date: " + m.group(1));
			}
		}
		catch (Exception e)
		{
			log.error("There was an error parsing the date for: " + regexDate, e);
			e.printStackTrace();
		}
	}

	@Override
	public String toString()
	{
		return "Document{" +
				"documentText='" + documentText + '\'' +
				", comedians=" + comedians +
				'}';
	}
}
