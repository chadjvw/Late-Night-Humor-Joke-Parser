package net.chadjvw.gis;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import net.chadjvw.gis.data.Comedian;
import net.chadjvw.gis.data.Component;
import net.chadjvw.gis.data.Document;
import net.chadjvw.gis.data.Joke;

/**
 * Created by Chad on 5/13/2016.
 */
public class Parser
{
	private static final Logger log = LogManager.getLogger(Parser.class);
	private static final Pattern comedianPat = Pattern.compile("\\s+([0-9]{1,4}\\sof\\s[0-9]{1,4}\\sDOCUMENTS.+?(?=LANGUAGE: ENGLISH))", Pattern.DOTALL);
	private static final Pattern documentPat = Pattern.compile("\\s+([0-9]{1,4}\\sof\\s[0-9]{1,4}\\sDOCUMENTS.+?(?=LANGUAGE: ENGLISH))", Pattern.DOTALL);

	private Parser()
	{
		// hide
	}

	public static Set<Document> parseDocument(String file)
	{
		Set<Document> documents = new HashSet<>();

		int counter = 0;


		Matcher matcher;

		matcher = documentPat.matcher(file);

		while (matcher.find())
		{
			Document document = new Document(matcher.group(1));
			documents.add(document);
			counter++;
		}

		log.info("Processed " + counter + " documents.");
		return documents;
	}

	public static ArrayList<Component> parseComedianSet(String file, Date date)
	{
		ArrayList<Component> comedians = new ArrayList<>();

		int counter = 0;


		Matcher matcher;

		matcher = comedianPat.matcher(file);

		while (matcher.find())
		{
			String regexComedian = matcher.group(1);
			String regexJoke = matcher.group(2);

			for (Component comedian : comedians)
			{
				if (comedian.getName().equals(regexComedian))
				{
					int i = 0;
					i = comedians.indexOf(comedian);
					comedian = comedians.get(i);
					comedian.add(new Joke(regexJoke, date));
				}
				else
				{
					comedians.add(new Comedian(regexComedian));
					int i = 0;
					i = comedians.indexOf(new Comedian(regexComedian));
					comedian = comedians.get(i);
					comedian.add(new Joke(regexJoke, date));

				}
			}

			counter++;
		}
		return null;
	}
}
