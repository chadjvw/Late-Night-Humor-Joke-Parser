package net.chadjvw.gis;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import net.chadjvw.gis.data.Comedian;
import net.chadjvw.gis.data.Component;
import net.chadjvw.gis.data.Document;

/**
 * Created by Chad on 5/12/2016.
 */
public class JokeParser
{
	private static final Logger log = LogManager.getLogger(JokeParser.class);
	private Component allComedians = new Comedian("ALL");

	public static void main(String[] args) throws IOException
	{
		File jokeDir = new File("D:\\temp\\joke_parser\\jokes");

		File[] jokeFiles = jokeDir.listFiles();
		log.info("Found " + jokeFiles.length + " files to process.");

		Set<Document> allDocuments = new HashSet<>();

		for (File jokeTxt : jokeFiles)
		{
			String joke = FileUtils.readFileToString(jokeTxt);

			allDocuments.addAll(Parser.parseDocument(joke));
		}

		allDocuments.forEach(document -> System.out.println(document.toString()));

		log.info("Done.");
	}
}
